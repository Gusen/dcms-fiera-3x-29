<?
include_once '../sys/inc/start.php';
include_once '../sys/inc/compress.php';
include_once '../sys/inc/sess.php';
include_once '../sys/inc/home.php';
include_once '../sys/inc/settings.php';
$temp_set=$set;
include_once '../sys/inc/db_connect.php';
include_once '../sys/inc/ipua.php';
include_once '../sys/inc/fnc.php';
include_once '../sys/inc/adm_check.php';
include_once '../sys/inc/user.php';
user_access('adm_set_user',null,'index.php?'.SID);
adm_check();

$set['title']='Пользовательские настройки';
include_once '../sys/inc/thead.php';
title();
if (isset($_POST['save']))
{
if ($_POST['write_guest']==1 || $_POST['write_guest']==0)$temp_set['write_guest']=intval($_POST['write_guest']);
if ($_POST['guest_select']==1 || $_POST['guest_select']==0)$temp_set['guest_select']=intval($_POST['guest_select']);
if ($_POST['aut_ref']==1 || $_POST['aut_ref']==0)$temp_set['aut_ref']=intval($_POST['aut_ref']);
if ($_POST['reg_ref']==1 || $_POST['reg_ref']==0)$temp_set['reg_ref']=intval($_POST['reg_ref']);
if ($_POST['user_collision_online']==1 || $_POST['user_collision_online']==0)$temp_set['user_collision_online']=intval($_POST['user_collision_online']);
$temp_set['user_online']=intval($_POST['user_online']);
$temp_set['system_ank']=intval($_POST['system_ank']);
$temp_set['user_reg_time']=intval($_POST['user_reg_time']);
$temp_set['medals_us_set']=intval($_POST['medals_us_set']);
$temp_set['medals_us_set_s']=my_esc($_POST['medals_us_set_s']);
$temp_set['reg_nick_mat'] = intval($_POST['reg_nick_mat']);
$temp_set['p_str']=intval($_POST['p_str']);
$temp_set['reg_select']=esc($_POST['reg_select']);

$temp_set['banbase']=intval($_POST['banbase']);


if (save_settings($temp_set))
{
	admin_log('Настройки','Пользователи',"Изменение пользовательских настроек");
	mysql_query("ALTER TABLE `user` CHANGE `set_p_str` `set_p_str` INT( 11 ) DEFAULT '$temp_set[p_str]'");
	$_SESSION['message'] = lang('Настройки успешно приняты');
	exit(header('Location: ?'));
}
else
$err='Нет прав для изменения файла настроек';
}
err();
aut();



echo "<form method=\"post\" action=\"?\">\n";

echo "<div class='p_m'>Пунктов на страницу:<br /><select name='p_str'>";
echo "<option value='5'".($temp_set['p_str']==5?" selected='selected'":null)."> 5 пунктов</option>";
echo "<option value='7'".($temp_set['p_str']==7?" selected='selected'":null)."> 7 пунктов</option>";
echo "<option value='10'".($temp_set['p_str']==10?" selected='selected'":null)."> 10 пунктов</option>";
echo "<option value='15'".($temp_set['p_str']==15?" selected='selected'":null)."> 15 пунктов</option>";
echo "<option value='20'".($temp_set['p_str']==20?" selected='selected'":null)."> 20 пунктов</option>";
echo "<option value='25'".($temp_set['p_str']==25?" selected='selected'":null)."> 25 пунктов</option>";
echo "<option value='30'".($temp_set['p_str']==30?" selected='selected'":null)."> 30 пунктов</option>";

echo "</select>
<br /> * Учитывайте что чем больше пунктов тем больше запросов на страницу 
<br /> * Рекомендуется не более 10 пунктов 
<br /> * данные настройки действуют до момента авторизации и записываются в настройки после регистрации 
</div>";



echo "<div class='p_m'>Показ возможных ников в онлайне:<br />\n<select name=\"user_collision_online\">\n";
if ($temp_set['user_collision_online']==1)$sel=' selected="selected"';
else $sel=NULL;
echo "<option value=\"1\"$sel>Показывать</option>\n";
if ($temp_set['user_collision_online']==0)$sel=' selected="selected"';
else $sel=NULL;
echo "<option value=\"0\"$sel>Скрывать</option>\n";
echo "</select><br />







*	использование функции грузит файл не слишком ,но на слабых хостингах лучше не использовать. да и вид портит 
<br />* В анкете по прежнему остается включенным </div>
";




echo "
<div class='p_m'>Время пока юзер считается онлайн : <br />
<select name='user_online'>
<option value='300'".($temp_set['user_online']==300?" selected='selected'":null)."> 5 минут</option>
<option value='600'".($temp_set['user_online']==600?" selected='selected'":null)."> 10 минут</option>
<option value='1200'".($temp_set['user_online']==1200?" selected='selected'":null).">20 минут</option>
<option value='1800'".($temp_set['user_online']==1800?" selected='selected'":null)."> 30 минут </option>
<option value='3600'".($temp_set['user_online']==3600?" selected='selected'":null).">1 час</option>
</select>
<br />* желательно ставить не более 20 минут ,иначи юзеры начнут думать что это боты седят 
</div>
";









echo "<div class='p_m'>Время пока юзер считается новым на сайте : <br />
<select name='user_reg_time'>
<option value='43200'".($temp_set['user_reg_time']==43200?" selected='selected'":null)."> 12 часов</option>
<option value='86400'".($temp_set['user_reg_time']==86400?" selected='selected'":null)."> 1 день</option>
<option value='172800'".($temp_set['user_reg_time']==172800?" selected='selected'":null)."> 2  дня</option>
<option value='259200'".($temp_set['user_reg_time']==259200?" selected='selected'":null)."> 3 дня</option>
<option value='345600'".($temp_set['user_reg_time']==345600?" selected='selected'":null)."> 4 дня </option>
<option value='432000'".($temp_set['user_reg_time']==432000?" selected='selected'":null)."> 5 дня</option>
<option value='518400'".($temp_set['user_reg_time']==518400?" selected='selected'":null)."> 6 дня</option>
<option value='604800'".($temp_set['user_reg_time']==604800?" selected='selected'":null)."> 1 неделя</option>
	</select>
	<br />* пока юзер новый у него особая иконка на сайте

</div>
";





echo "<div class='p_m'>Мат в никах при регистрации :<br />\n<select name=\"reg_nick_mat\">\n";
if ($temp_set['reg_nick_mat']==1)$sel=' selected="selected"';else $sel=NULL;
echo "<option value=\"1\"$sel>Включен</option>\n";
if ($temp_set['reg_nick_mat']==0)$sel=' selected="selected"';else $sel=NULL;
echo "<option value=\"0\"$sel>Отключен</option>\n";
echo "</select></div>";






echo " <div class='p_m'>Режим регистрации:<br />\n<select name=\"reg_select\">\n";
echo "<option value=\"close\">Закрыта</option>\n";
if ($temp_set['reg_select']=='open')$sel=' selected="selected"';else $sel=NULL;
echo "<option value=\"open\"$sel>Открыта</option>\n";
if ($temp_set['reg_select']=='open_mail')$sel=' selected="selected"';else $sel=NULL;
echo "<option value=\"open_mail\"$sel>Открыта + E-mail</option>\n";
echo "</select></div>";

echo " <div class='p_m'>Режим гостя:<br />\n<select name=\"guest_select\">\n";
echo "<option value=\"0\">Открыто все</option>\n";
if ($temp_set['guest_select']=='1')$sel=' selected="selected"';else $sel=NULL;
echo "<option value=\"1\"$sel>Закрыто все *</option>\n";
echo "</select><br /> * остаются открытыми регистрация и авторизация</div>";


echo "<div class='p_m'>Медальки юзеров :<br />\n<select name=\"medals_us_set\">\n";
if ($temp_set['medals_us_set']==1)$sel=' selected="selected"';else $sel=NULL;
echo "<option value=\"1\"$sel>Включены</option>\n";
if ($temp_set['medals_us_set']==0)$sel=' selected="selected"';else $sel=NULL;
echo "<option value=\"0\"$sel>Отключены</option>\n";
echo "</select></div>";





echo " <div class='p_m'>Медальки юзеров настройка :<br />\n<select name=\"medals_us_set_s\">\n";
if ($temp_set['medals_us_set_s']=='rating')$sel=' selected="selected"';else $sel=NULL;
echo "<option value=\"rating\"$sel>По рейтингу</option>\n";
if ($temp_set['medals_us_set_s']=='balls')$sel=' selected="selected"';else $sel=NULL;
echo "<option value=\"balls\"$sel>По баллам</option>\n";
echo "</select>
 <br /> * таблицу медалик смотите в коде)) 
 <br /> * P.s либо по рейтингу либо по баллам 
</div>";





/*
- наверно выпилю
echo " <div class='p_m'>Показ away:<br />\n<select name=\"show_away\">\n";
if ($temp_set['show_away']==1)$sel=' selected="selected"';else $sel=NULL;
echo "<option value=\"1\"$sel>Показывать</option>\n";
if ($temp_set['show_away']==0)$sel=' selected="selected"';else $sel=NULL;
echo "<option value=\"0\"$sel>Скрывать</option>\n";
echo "</select></div>";
*/





echo " <div class='p_m'>Пишут в гостевой:<br />\n<select name=\"write_guest\">\n";
if ($temp_set['write_guest']==1)$sel=' selected="selected"';else $sel=NULL;
echo "<option value=\"1\"$sel>Все</option>\n";
if ($temp_set['write_guest']==0)$sel=' selected="selected"';else $sel=NULL;
echo "<option value=\"0\"$sel>Авторизованые</option>\n";
echo "</select>

<br />* \"все\", т.е юзеры как обычно ,а гости раз в 5 минут (с капчей)


</div>";


echo " <div class='p_m'>При входе в анкету системы :<br />\n<select name=\"system_ank\">\n";
if ($temp_set['system_ank']==1)$sel=' selected="selected"';else $sel=NULL;
echo "<option value=\"1\"$sel>Кидать на главную</option>\n";
if ($temp_set['system_ank']==0)$sel=' selected="selected"';else $sel=NULL;
echo "<option value=\"0\"$sel>Пропускать в анкету </option>\n";
echo "</select></div>";






echo " <div class='p_m'>Возврат при регистрации :<br />\n<select name=\"reg_ref\">\n";
if ($temp_set['reg_ref']==1)$sel=' selected="selected"';else $sel=NULL;
echo "<option value=\"1\"$sel>Включен</option>\n";
if ($temp_set['reg_ref']==0)$sel=' selected="selected"';else $sel=NULL;
echo "<option value=\"0\"$sel>Отключить</option>\n";
echo "</select><br />
*После регистрации на сайте ,юзер вернет на страницу до того как он попал на страницу регистрации и выведет сообщение
 ,например было он в альбомах вернет его в альбомы  и т.п (beta)</div>";



echo "  <div class='p_m'>Возврат при авторизации :<br />\n<select name=\"aut_ref\">\n";
if ($temp_set['aut_ref']==1)$sel=' selected="selected"';else $sel=NULL;
echo "<option value=\"1\"$sel>Включен</option>\n";
if ($temp_set['aut_ref']==0)$sel=' selected="selected"';else $sel=NULL;
echo "<option value=\"0\"$sel>Отключить</option>\n";
echo "</select><br />
*После авторизации на сайте ,
юзер вернет на страницу до того как он попал на страницу авторизации  .<br />
* при этом юзерa не кинет на стартовую страницу (beta)</div>";





echo " <div class='p_m'>Искать нарушителей в banbase :<br />\n<select name=\"banbase\">\n";
if ($temp_set['banbase']==1)$sel=' selected="selected"';else $sel=NULL;
echo "<option value=\"1\"$sel>Включить</option>\n";
if ($temp_set['banbase']==0)$sel=' selected="selected"';else $sel=NULL;
echo "<option value=\"0\"$sel>Выключить</option>\n";
echo "</select></div>";



echo " <div class='p_m'><input value=\"Сохранить\" name='save' type=\"submit\" />\n";
echo "</form></div>";

if (user_access('user_mass_delete'))
echo "<div class='foot'>&raquo;<a href='/adm_panel/delete_users.php'>Удаление пользователей</a></div>";


echo "<div class='foot'>\n";
echo "&laquo;<a href='/adm_panel/'>В админку</a><br />\n";
echo "</div>\n";

include_once '../sys/inc/tfoot.php';
?>